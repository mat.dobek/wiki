# Tmux

```
tmux new -s netguru
tmux ls
tmux a(ttach) -t 0
tmux a(ttach) -t database

prefix + d = detach
prefix + c = create window

prefix + z = fullzcreen
prefix + , = rename window

prefix + p = previous window
prefix + n = next window
prefix + 1 = window nr 1

prefix + [ = navigation mode

prefix + { = move the current pane to the previous position
prefix + } = move the current pane to the next position

swap-window -t 0 = swap current window with one on 0 position
```

