## size vs length vs count

* if you already load all entries, say User.all, then you should use length to avoid another db query
* if you haven't anything loaded, use count to make a count query on your db
* if you don't want to bother with these considerations, use size which will adapt

## Hooks

By default - do not use hooks.

Good:
```
def index
  # stuff
end

def show
  # stuff
end

def update
  validate_payment!

  # stuff
end

def create
  validate_payment!

  # stuff
end

private

def validate_payment!
  # stuff
end

```


Its use is justified only if your are certain that given hook should be use by default by all actions. Example: authentication.
Thus do not use `only` keyword.


Good:
```
#before_action :authenticate_user!, only: [:show, :create]
before_action :authenticate_user!, except: [index]

def index
end

def show
end

def create
end

def delete
end
```
