# Minitest

### Freeze time
```
travel_to DateTime.new(2000, 1, 1) do
end
```

### Test exceptions
```
assert_raises ArgumentError do
    bar.do_it
end
```

### Doubles
```
fake_mailer = Object.new
def fake_mailer.deliver_later; end
::Mailer.stubs(:payment_approved).once.returns(fake_mailer)
```

### stubs

```
ActiveRecord::Base.any_instance.stubs(:xxx).returns()

ActiveRecord::Base.any_instance.stubs(:xxx).once.returns()
ActiveRecord::Base.any_instance.stubs(:xxx).at_least_once.returns()
ActiveRecord::Base.any_instance.stubs(:xxx).times(2).returns()
ActiveRecord::Base.any_instance.stubs(:xxx).never.returns()
```

### `assert_changes`
[Rails 5.1 introduced assert_changes and assert_no_changes | BigBinary Blog](https://blog.bigbinary.com/2017/05/09/rails-5-1-introduced-assert_changes-and-assert_no_changes.html)

```
assert_no_changes -> { payment.reload } do
  described_class.new(payment: payment).call
end
```

```
assert_difference 'Article.count', -1 do
  post :create, params: { article: {...} }
end
```

### Controllers

```
 get :index, params: { address: "Basel" }
```

You also have access to three instance variables in your functional tests, after a request is made:
@controller - The controller processing the request
@request - The request object
@response - The response object

```
assert_response :success
```
