# Nginx tutorial: Basics concepts [1/3]
## What is Nginx?

Nginx was originally created as a web server, to solve the [C10k](https://en.wikipedia.org/wiki/C10k_problem) problem. And as one, it can serve your data with blazing speed. But Nginx is so much more than just a web server. You can use it as a reverse proxy, making an easy integration with slower upstream servers (like Unicorn, or Puma). You can distribute your traffic properly (load balancer), stream media, resize your images on the fly, cache content and so much more.

The basic nginx architecture consist of master process and its workers. Master is supposed to read the configuration file, and maintain worker processes, while workers do the actual processing of requests.










## Base commands

To start nginx, you simply type:

```
[sudo] nginx
```

While your nginx instance is running, you can manage it, by sending proper signals:

```
[sudo] nginx -s signal
```

Available signals:

* `stop` - fast shutdown
* `quit` - graceful shutdown (wait for workers to finish their processes)
* `reload` - reload the configuration file
* `reopen` - reopening the log files










## Directive and Context

The nginx configuration file, can by found by default in:

* `/etc/nginx/nginx.conf`,
* `/usr/local/etc/nginx/nginx.conf`, or
* `/usr/local/nginx/conf/nginx.conf`

This file consists of:

* directive - option, that contains of name and parameters, ended with semicolon

```
gzip on;
```

* context - section, that you can declare directives - similar to scope in programming languages

```
worker_processes 2; # directive in global context

http {              # http context
	gzip on;          # directive in http context

  server {          # server context
    listen 80;      # directive in server context
  }
}
```










## Directive types

You have to care when using the same directive in multiple contexts, as inheritance model differs, for different directives. There are 3 types of directives, each with its own inheritance model.



### Normal

Have one value per context. Also, it can be defined only once in the context. Subcontexts can override parent directive - this override will be valid only in given subcontext.

```
gzip on;
gzip off; # illegal to have 2 normal directives in same context

server {
  location /downloads {
    gzip off;
  }

  location /assets {
    # gzip is on here
  }
}
```



### Array

Adding multiple directives in the same context, will add to the values, instead of overwriting them altogether. Defining directive in subcontext will override ALL parent values, in given subcontext.

```
error_log /var/log/nginx/error.log;
error_log /var/log/nginx/error_notive.log notice;
error_log /var/log/nginx/error_debug.log debug;

server {
  location /downloads {
    # this will override all the parent directives
    error_log /var/log/nginx/error_downloads.log;
  }
}
```



### Action directive

Actions are directives that change things. It inheritance behaviour may depend, according to the module.

For example in case of  `rewrite`  directive, every matching one will be executed:

```
server {
  rewrite ^ /foobar;

  location /foobar {
    rewrite ^ /foo;
    rewrite ^ /bar;
  }
}
```

If the user will try to fetch `/sample`:
* server rewrite is executed, rewriting from `/sample`, to `/foobar`
* location `/foobar` is matched
* first location rewrite is executed, rewriting from `/foobar `, to `/foo`
* second location rewrite is executed, rewriting from `/foo`, to `/bar`

That is different behaviour that `return` directive provides:

```
server {
  location / {
    return 200;
    return 404;
  }
}
```

In above case `200` status is returned immediately.










## Processing request

Inside nginx, you can specify multiple virtual servers, each described by `server { }` context.

```
server {
  listen      *:80 default_server;
  server_name netguru.co;

  return 200 "Hello from netguru.co";
}

server {
  listen      *:80;
  server_name foo.co;

  return 200 "Hello from foo.co";
}

server {
  listen      *:81;
  server_name bar.co;

  return 200 "Hello from bar.co";
}
```

This will give nginx some insights how to handle incoming requests. Nginx will first check the `listen` directive to test which virtual server is listening on given IP:port combination.
Then the value from `server_name` directive, is tested against `Host` header (which stores domain name of the server).

Nginx will choose virtual server in following order:
1. Server listing on IP:port, with a matching `server_name` directive
2. Server listing on IP:port, with `default_server` flag
3. Server listing on IP:port, first one defined
4. If no matches - refuse the connection.

In example above that means:

```
Request to foo.co:80     => "Hello from foo.co"
Request to www.foo.co:80 => "Hello from netguru.co"
Request to bar.co:80     => "Hello from netguru.co"
Request to bar.co:81     => "Hello from bar.co"
Request to foo.co:81     => "Hello from bar.co"
```



### `server_name` directive

`server_name` directive accepts multiple values. It also handles wildcard matching, and regular expressions.

```
server_name netguru.co www.netguru.co; # exact match
server_name *.netguru.co;              # wildcard matching
server_name netguru.*;                 # wildcard matching
server_name  ~^[0-9]*\.netguru\.co$;   # regexp matching
```

When there is ambiguity, nginx uses following order:
1. Exact name
2. Longest wildcard name starting with an asterisk, e.g. "*.example.org"
3. Longest wildcard name ending with an asterisk, e.g. "mail.*"
4. First matching regular expression (in order of appearance in a configuration file)

Nginx will store 3 hash tables: exact names, wildcards starting with asterisk, and wildcards ending with asterisk. If the result is not in any of that tables, regular expression will be tested sequentially.

It is worth keeping in mind, that

```
server_name .netguru.co;
```

Is an abbreviation from

```
server_name  netguru.co  www.netguru.co  *.netguru.co;
```

With one difference: `.netguru.co` is stored in the second table, which means that it is a bit slower than explicit declaration.



### `listen` directive

In most cases, you’ll find  `listen` directive, accepting IP:port values

```
listen 127.0.0.1:80;
listen 127.0.0.1;    # by default port :80 is used

listen *:81;
listen 81;           # by default all ips are used

listen [::]:80;      # IPv6 addresses
listen [::1];        # IPv6 addresses
```

However, it is also possible to specify UNIX-domain sockets

```
listen unix:/var/run/nginx.sock;
```

You can even use hostname

```
listen localhost:80;
listen netguru.co:80;
```

But this should be used with caution, as hostname may not be resolved on nginx start, causing nginx to be unable to bind on given TCP socket.

Finally, if the directive is not present then `*:80` is used.










## Minimal configuration

With all that knowledge - we should be able to create, and understand minimal configuration needed for running nginx.

```
# /etc/nginx/nginx.conf

events {} 					# events context needs to be defined to consider config valid

http {
 server {
    listen 80;
    server_name  netguru.co  www.netguru.co  *.netguru.co;

	  return 200 "Hello";
  }
}
```










## `root`, `location`, and `try_files` directives
### `root` directive

`root` directive sets the root directory for requests, allowing nginx to map incoming request on to the file system.

```
server {
  listen 80;
  server_name netguru.co;
  root /var/www/netguru.co;
}
```

Which allows nginx to server content, according to given request

```
netguru.co:80/index.html     # returns /var/www/netguru.co/index.html
netguru.co:80/foo/index.html # returns /var/www/netguru.co/foo/index.html
```



### `location` directive

`location` directive sets configuration, depending  on requested URI.
`location [modifier] path`

```
location /foo/ {
  # ...
}
```

When no modifier specified, the path is treated as prefix, after which anything can follow.
Above example will match

```
/foo
/fooo
/foo123
/foo/bar/index.html
...
```

Also, multiple `location` directives can be used in given context

```
server {
  listen 80;
  server_name netguru.co;
  root /var/www/netguru.co;

  location / {
    return 200 "root";
  }

  location /foo/ {
    return 200 "foo";
  }
}
```

```
netguru.co:80	/       # => "root"
netguru.co:80	/foo    # => "foo"
netguru.co:80	/foo123 # => "foo"
netguru.co:80	/bar    # => "root"
```

Nginx also provide few modifiers, that can be used in conjunction with `location`. Those modifiers impact which location block will be used, as each modifier, has assigned precedence.

```
=           - Exact match
^~          - Preferential match
~ && ~*     - Regex match
no modifier - Prefix match
```

Nginx will first check any exact matches. If not found, we’ll look for preferential ones. If this match will also fail, regex matches will be tested in order of their appearance. And at least, last prefix match will be used.

```
location /match {
  return 200 'Prefix match: matches everything that starting with /match';
}

location ~* /match[0-9] {
  return 200 'Case insensitive regex match';
}

location ~ /MATCH[0-9] {
  return 200 'Case sensitive regex match';
}

location ^~ /match0 {
  return 200 'Preferential match';
}

location = /match {
  return 200 'Exact match';
}
```

```
/match/    # => 'Exact match'
/match0    # => 'Preferential match'
/match1    # => 'Case insensitive regex match'
/MATCH1    # => 'Case sensitive regex match'
/match-abc # => 'Prefix match: matches everything that starting with /match'
```

### `try_files` directive

Try different paths, returning whichever one is found.

```
try_files $uri index.html =404;
```

So for `/foo.html` request, it will try to return files in following order:
1. $uri ( /foo.html )
2. index.html
3. If none is found - 404.



What's interesting, if we define `try_files` in `server` context, and then define location that matches all requests - our `try_files` will not be executed.
That’s because `try_files` in `server` context defines its own pseudo-location, that is the least specific location possible. So defining `location /` will be more specific than our pseudo-location.

```
server {
  try_files $uri /index.html =404;

  location / {
  }
}
```

Thus, you should avoid `try_files` in `server` context:

```
server {
  location / {
    try_files $uri /index.html =404;
  }
}
```










# Nginx tutorial: Performance [2/3]
## `tcp_nodelay`, `tcp_nopush`, and `sendfile`
### `tcp_nodelay`

In the early days of TCP, engineers were facing with a danger of congestion collapse. Quite a few solutions emerged as a prevention, and one of them, was algorithm proposed by John Nagle.

Nagle's algorithm aims to prevent being overwhelmed with a great number of small packages. It does not interfere with full size TCP packages (Maximum Segment Size, or MSS in short). Only with packets that are smaller than MSS. Those packages will be transmitted only if receiver successfully sends back all the acknowledgements of previous packages (ACKs). And during waiting, sender can buffer more data.

```
if package.size >= MSS.size
  send(package)
elsif acks.all_received?
  send(package)
else
  # acumulate data
end
```

During that time, another proposal emerged: the Delayed ACK.

In TCP communication we are sending data, and receiving acknowledgements (ACK) - which tells us that those data were delivered successfully.
Delayed ACK tries to resolve issue where wire is flood by massive number of ACK packages. To cut this number, receiver will wait for some data to be send back to the sender, and include ACK packages with those data. If there is no data to be send back, we have to send ACKs at least every 2 * MSS, or every 200 - 500 ms (in case we are no longer receiving packages).

```
if packages.any?
  send
elsif last_ack_send_more_than_2MSS_ago? || 200_ms_timer.finished?
  send
else
  # wait
end
```

As you may start noticing - this may led to some temporary deadlocks, on persisted connection. Lets reproduce it!

Assumptions:
* initial congestion window is equal 2. Congestion window is part of another TCP mechanism, called Slow-Start. The details are not important right now, just keep in mind that it restricts how many packages can be send at once. In first round-trip we are allowed to send 2 MSS packages. In second: 4 MSS packages, in third: 8 MSS, and so on.
* 4 buffered packages, waiting to be send: A, B, C, D
* A, B, C are MSS packages
* D is a small package

Scenario:
* Due to initial congestion window, sender is allowed to transmit two packages: A and B.
* Receiver, upon successful getting both packages, is sending ACK.
* Sender transmits C package. However Nagle holds him from sending D (package is too small, wait for the ACK from C)
* On receiver side, Delayed ACK holds him from sending ACK(which is send every 2 packages, or every 200 ms)
* After 200ms, receiver sends ACK for C package.
* Sender receives ACK, and sends D package.

During this exchange 200ms lag was introduced, due to deadlock between Nagel and Delayed ACK.

Nagle algorithm was a true saviour in it's time, and still provides [great value](https://news.ycombinator.com/item?id=9048947). However, in most cases we won't need it for our website, thus it can be safely turn down via adding `TCP_NODELAY` flag.

```
tcp_nodelay on;     # sets TCP_NODELAY flag, used on keep-alive connection
```

Enjoy your 200 ms gain!


To get some nitpicky details, I encourage reading [this great paper](http://ccr.sigcomm.org/archive/2001/jan01/ccr-200101-mogul.pdf).



### `sendfile`

Normally, when a file needs to be sent, following steps are required:
* malloc(3) - allocate a local buffer, for storing the object data
* read(2) - retrieve and copy the object into the local buffer
* write(2) - copy object from the local buffer into the socket buffer

This involves 2 context switches (read, write), and makes unnecessary, second copy of the same object. As you may see, it is not the optimal way. Thankfully there is another system call, that improves sending files, and it's called (surprise surprise): sendfile(2). This call retrieves an object to the file cache, and passes the pointers (without copying the whole object), straight to the socket descriptor. Netflix states, that using `sendfile(2)` increased network throughput [from 6Gbps to 30Gbps](https://people.freebsd.org/~rrs/asiabsd_2015_tls.pdf).

However, `sendfile(2)` got some caveats:
* does not work with UNIX sockets (e.g. when serving static files through your upstream server)
* can perform differently, depending on the operating system (more [here](https://blog.phusion.nl/2015/06/04/the-brokenness-of-the-sendfile-system-call/))

To turn this on nginx

```
sendfile on;
```



### `tcp_nopush`

`tcp_nopush` is opposite to `tcp_nodelay`. Instead of pushing packages as fast as possible - it aims to optimize the amount of data sends at once.
It will force to wait for the packages to get it's maximum size (MSS), before sending it to the client. And this directive only works, when `sendfile` is on.

```
sendfile on;
tcp_nopush on;
```

It may appear that `tcp_nopush` and `tcp_nodelay` are mutually exclusive. But if all 3 directives are turned on, nginx will:
* ensure packages are full, before sending to the client
* for the last packet, `tcp_nopush` will be removed - allowing TCP to send immediately, without 200ms delay



### Config

```
sendfile on;
tcp_nopush on;
tcp_nodelay on;
```










## How many processes should I have?
### Worker processes

`worker_process` directive defines, how many workers should be run. By default, this value is set to 1. Safest setting is to use number of cores, by passing `auto` option.
But still, due to Nginx architecture, which handle requests blazingly fast - we probably won't use more than 2 - 4 processes at a time (unless you are hosting Facebook, or doing some CPU intensive stuff inside nginx).

```
worker_process auto;
```



### Worker connections

Directive that is directly tied with `worker_process` is `worker_connections`. It specifies how many connection at once, can be opened by a worker process. This number includes all connections (e.g. connections with proxied servers), and not only connections with clients. Also, it is worth keeping in mind, that one client can open multiple connections, to fetch other resources simultaneously.

```
worker_connections 1024;
```


### Open files limit

"Everything is a file" in Unix based systems. It means that documents, directories, pipes or even sockets are files. And system has a limitation, how many files can be opened at once, by a process. To check the limits:

```
ulimit -Sn		# soft limit
ulimit -Sn		# hard limit
```

This system limit, must be tweaked accordingly to `worker_connections`. Any incoming connection opens at least one file (usually two - connection socket and either backend connection socket or static file on disk). So it is safe to have this value equal to `worker_connections` * 2. Nginx, fortunately provide option for increasing this system value, withing nginx config. To do so add `worker_rlimit_nofile` directive with proper number, and reload the nginx.

```
worker_rlimit_nofile 2048;
```



### Config

```
worker_process auto;
worker_rlimit_nofile 2048; # Changes the limit on the maximum number of open files (RLIMIT_NOFILE) for worker processes.
worker_connections 1024;   # Sets the maximum number of simultaneous connections that can be opened by a worker process.
```



### Maximum number of connections

Given above, we can calculate how many concurrent connections we can handle at once:

```
max no of connections =

    worker_processes * worker_connections
----------------------------------------------
 (keep_alive_timeout + avg_response_time) * 2
```

`keep_alive_timeout` (more on that later) + `avg_response_time` tells us, how long single connection is opened. We also divide by 2, as usally, you will have 2 connections opened, by one client: one between nginx and client, and second between nginx and upstream server.










## Gzip

Enabling `gzip` should significantly reduce the weight of your response, thus it will appear faster on the client side.

### Compression level

Gzip has different level of compressions, from 1 to 9. Incrementing this level will reduce the size of file, but also increase resources consumption. As a standard we keep this number at 3 - 5, as going above it will gain small saves, simultaneously with larger CPU usage.

Here is example with compressing file with gzip, with different levels. 0 stands for uncompressed file.

```
curl -I -H 'Accept-Encoding: gzip,deflate' https://netguru.co/
```

```
❯ du -sh ./*
 64K    ./0_gzip
 16K    ./1_gzip
 12K    ./2_gzip
 12K    ./3_gzip
 12K    ./4_gzip
 12K    ./5_gzip
 12K    ./6_gzip
 12K    ./7_gzip
 12K    ./8_gzip
 12K    ./9_gzip

❯ ls -al
-rw-r--r--   1 matDobek  staff  61711  3 Nov 08:46 0_gzip
-rw-r--r--   1 matDobek  staff  12331  3 Nov 08:48 1_gzip
-rw-r--r--   1 matDobek  staff  12123  3 Nov 08:48 2_gzip
-rw-r--r--   1 matDobek  staff  12003  3 Nov 08:48 3_gzip
-rw-r--r--   1 matDobek  staff  11264  3 Nov 08:49 4_gzip
-rw-r--r--   1 matDobek  staff  11111  3 Nov 08:50 5_gzip
-rw-r--r--   1 matDobek  staff  11097  3 Nov 08:50 6_gzip
-rw-r--r--   1 matDobek  staff  11080  3 Nov 08:50 7_gzip
-rw-r--r--   1 matDobek  staff  11071  3 Nov 08:51 8_gzip
-rw-r--r--   1 matDobek  staff  11005  3 Nov 08:51 9_gzip
```



### `gzip_http_version 1.1;`

This directive tells nginx to use gzip only for HTTP 1.1 and above. We don't include HTTP 1.0 here, as, for 1.0 version, it is impossible to use both keep-alive and gzip. So you have to decide which one you prefer: HTTP 1.0 clients missing out on gzip, or HTTP 1.0 clients missing out on keep-alive.



### Config

```
gzip on;               # enable gzip
gzip_http_version 1.1; # turn on gzip for http 1.1 and above
gzip_disable "msie6";  # IE 6 had issues with gzip
gzip_comp_level 5;     # inc compresion level, and CPU usage
gzip_min_length 100;   # minimal weight to gzip file
gzip_proxied any;      # enable gzip for proxied requests (e.g. CDN)
gzip_buffers 16 8k;    # compression buffers (if we exceed this value, disk will be used instead of RAM)
gzip_vary on;          # add header Vary Accept-Encoding (more on that in Caching section)

# define files which should be compressed
gzip_types text/plain;
gzip_types text/css;
gzip_types application/javascript;
gzip_types application/json;
gzip_types application/vnd.ms-fontobject;
gzip_types application/x-font-ttf;
gzip_types font/opentype;
gzip_types image/svg+xml;
gzip_types image/x-icon;
```










## Caching

Caching is another thing, that can speed up requests nicely, for returning users.

Managing the cache can be controlled just by 2 headers:
* `Cache-Control` for managing cache in HTTP/1.1
* `Pragma` for backwards compatibility with HTTP/1.0 clients

Caches itself can be grouped in 2 categories: public and private caches. Public cache store responses for reuse for more than one user. A private cache is dedicated to a single user. We can easly define, which cache should be used:

```
add_header Cache-Control public;
add_header Pragma public;
```

For standard assets we would also like to keep them for 1 month:

```
location ~* \.(jpg|jpeg|png|gif|ico|css|js)$ {
  expires 1M;
  add_header Cache-Control public;
  add_header Pragma public;
}
```

Configuration above seems to be sufficient. However there is one caveat, when using public cache.

Let’s see, what will happen if we would store our asset in public cache (e.g. CDN), with URI, as the only identifier. In this scenario we also assume that gzip is on.

We got 2 browsers:
* old one, which does not support gzip
* new one, which does support gzip

Old browser sends a request for a `netguru.co/style.css` to our CDN. As CDN does not have this resource yet, it will request our sever for it, and return uncompressed response. CDN stores file in the hash (for later usage):

```
{
  ...
  netguru.co/styles.css => FILE("/sites/netguru/style.css")
  ...
}
```

And return it to the client.

Now, the new browser sends the same request to the CDN, asking for `netguru.co/style.css`, expecting gziped resource. As CDN only identifies resources by the URI, it will return the same uncompressed resource for the new browser. New browser will try to extract non-gziped file, and will get garbage.

If we could tell somehow public cache to identify resource based on URI, and encoding - we could avoid this issue

```
{
  ...
  (netguru.co/styles.css, gzip) => FILE("/sites/netguru/style.css.gzip")
  (netguru.co/styles.css, text/css) => FILE("/sites/netguru/style.css")
  ...
}
``
```

And this is exactly what `Vary Accept-Encoding;` does. It tells public cache, that resource can be distinguished by URI and `Accept-Encoding` header.

So our final configuration will look like:

```
location ~* \.(jpg|jpeg|png|gif|ico|css|js)$ {
  expires 1M;
  add_header Cache-Control public;
  add_header Pragma public;
  add_header Vary Accept-Encoding;
}
```










## Timeouts

The `client_body_timeout`, and `client_header_timeout` defines how long nginx should wait for client to transmit body or header, before throwing the 408 (Request Time-out) error.

`send_timeout ` sets a timeout for transmitting a response to the client. The timeout is set only between two successive write operations, not for the transmission of the whole response. If the client does not receive anything within this time, the connection is closed.

Be careful when setting those values, as too large waiting time, can make you vulnerable to attackers, and too little will cut off slow clients.

```
# Configure timeouts
client_body_timeout   12;
client_header_timeout 12;
send_timeout          10;
```










## Buffers
### `client_body_buffer_size`

Sets buffer size for reading client request body. In case the request body is larger than the buffer, the whole body or only its part is written to a temporary file. For `client_body_buffer_size` , in most cases, setting 16k is enough.

This is yet another setting, which can have massive impact, but has to be used with care. Too little - and nginx will constantly use I/O to write remaining parts to the file. Too much - and you can be vulnerable to DOS attacks, when attacker could open all the connections, but you are not able to allocate buffer, on your system, to handle those connections.



### `client_header_buffer_size` and `large_client_header_buffers`

If headers don’t fit into `client_header_buffer_size`, then `large_client_header_buffers` will be used. If the request also won’t fit into that buffer, error is returned to the client. For most requests, a buffer of 1K bytes is enough. However, if a request includes long cookies, it may not fit into 1K.

If the size of a request line is exceed, the 414 (Request-URI Too Large) error is returned to the client.
If the size of a request header is exceed, the 400 (Bad Request) error is thrown.



### `client_max_body_size`

Sets the maximum allowed size of the client request body, specified in the “Content-Length” request header field. Depending if you want to allow users to upload files, tweak this configuration for your needs.



### Config

```
client_body_buffer_size       16K;
client_header_buffer_size     1k;
large_client_header_buffers   2 1k;
client_max_body_size          8m;
```










## Keep Alive

TCP protocol, on which HTTP is based, requires to perform three way handshake, to initiate connection. It means that before server can send you data (e.g. images), three full roundtrips between client and the server need to be made.
Assuming that you are requesting `/image.jpg` from Warsaw, and connecting to the nearest server in Berlin:

```
Open connection

TCP Handshake:
Warsaw  ->------------------ synchronize packet (SYN) ----------------->- Berlin
Warsaw  -<--------- synchronise-acknowledgement packet (SYN-ACK) ------<- Berlin
Warsaw  ->------------------- acknowledgement (ACK) ------------------->- Berlin

Data transfer:
Warsaw  ->---------------------- /image.jpg --------------------------->- Berlin
Warsaw  -<--------------------- (image data) --------------------------<- Berlin

Close connection
```

For another request, you will have to perform this whole initialisation, once again. If you sending multiple requests, during short periods of time, this can add up fast. And here keep alive comes handy. After successful response, it keeps the connection idle for given period of time (e.g. 10s). If another request will be made during this time, existing connection will be reused, and the idle time - refreshed.

Nginx provides few directives to tweak keep alive settings. Those can be grouped into two categories:
* keep-alive between client and nginx

```
keepalive_disable msie6;		# disable selected browsers.

# The number of requests a client can make over a single keepalive connection. The default is 100, but a much higher value can be especially useful for testing with a load‑generation tool, which generally sends a large number of requests from a single client.
keepalive_requests 100000;

# How long an idle keepalive connection remains open.
keepalive_timeout 60;
```

* keep-alive between nginx and upstream

```
upstream backend {
    # The number of idle keepalive connections to an upstream server that remain open for each worker process
    keepalive 16;
}

server {
  location /http/ {
    proxy_pass http://http_backend;
    proxy_http_version 1.1;
    proxy_set_header Connection "";
  }
}
```

Aaand that's it.











# Nginx tutorial: SSL Setup [3/3]
## Base SSL setup

In order to handle HTTPS traffic you need to have SSL certificate in place. You can check [Appendix::Let’s encrypt] to generate free certificate via Let’s encrypt.

When you have certificate, you can simply turn HTTPS by:
* specifying which protocols you would like to handle
* specifying which ciphers your server should use, and marking them as preferred
* listening on 443 port (this port is used by default, when you type 'https://' in your browser)
* providing path to certificate, and its key

```
ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
ssl_ciphers EECDH+CHACHA20:EECDH+AES128:RSA+AES128:EECDH+AES256:RSA+AES256:!MD5;
ssl_prefer_server_ciphers on;

server {
  listen 443 ssl default_server;
  listen [::]:443 ssl default_server;

  ssl_certificate /etc/nginx/ssl/netguru.crt;
  ssl_certificate_key /etc/nginx/ssl/netguru.key;
}
```









## OCSP Stapling

SSL certificate can be revoked at any time. Browsers in order to know if given certificate is no longer valid, need to perform additional query, via Online Certificate Status Protocol (OCSP). Instead of requiring users to perform given OCSP query, we could do it on the server, cache the result, and serve OCSP response to our clients, during TLS handshake. It is called OCSP stapling.

```
server {
  ssl_stapling on;
  ssl_stapling_verify on;                               # verify OCSP response
  ssl_trusted_certificate /etc/nginx/ssl/lemonfrog.pem; # tell nginx location of all intermediate certificates

  resolver 8.8.8.8 8.8.4.4 valid=86400s;                # resolution of the OCSP responder hostname
  resolver_timeout 5s;
}
```










## TLS Session Resumption

Using HTTPS, imposes TLS handshake, on top of TCP one. This increase significantly time, before actual data transfer is made. Assuming that you are requesting `/image.jpg` from Warsaw, and connecting to the nearest server in Berlin:

```
Open connection

TCP Handshake:
Warsaw  ->------------------ synchronize packet (SYN) ----------------->- Berlin
Warsaw  -<--------- synchronise-acknowledgement packet (SYN-ACK) ------<- Berlin
Warsaw  ->------------------- acknowledgement (ACK) ------------------->- Berlin

TLS Handshake:
Warsaw  ->------------------------ Client Hello  ---------------------->- Berlin
Warsaw  -<------------------ Server Hello + Certificate ---------------<- Berlin
Warsaw  ->---------------------- Change Ciper Spec -------------------->- Berlin
Warsaw  -<---------------------- Change Ciper Spec --------------------<- Berlin

Data transfer:
Warsaw  ->---------------------- /image.jpg --------------------------->- Berlin
Warsaw  -<--------------------- (image data) --------------------------<- Berlin

Close connection
```

To save one roundtrip during TLS handshake, and computational cost of generating new key, we could reuse session parameters generated during first request. Client and the server could store session parameters, behind the Session ID key. During the next TLS handshake, client can send the Session ID, and if the server will still have proper entry in cache - parameters generated during previous session, will be reused.

```
server {
	ssl_session_cache shared:SSL:10m;
	ssl_session_timeout 1h;
}
```



## Appendix :: Let's Encrypt
### Installation

Up to date can be found [here](https://certbot.eff.org/#ubuntuother-nginx).

For testing purposes use [staging environment](https://letsencrypt.org/docs/staging-environment/), to not exhaust [rate limits](https://letsencrypt.org/docs/rate-limits/).



### Generate new certificate

```
certbot certonly --webroot --webroot-path /var/www/netguru/current/public/  \
          -d foo.netguru.co \
          -d bar.netguru.co
```

Make sure it can be properly renewed

```
certbot renew --dry-run
```

Make sure you have automatic renewing added to crontab. Run `crontab -e`, and add following line

```
0 3 * * * /usr/bin/certbot renew --quiet --renew-hook "/usr/sbin/nginx -s reload"
```

Check if SSL is working properly on: [ssllabs](https://www.ssllabs.com/ssltest/)










