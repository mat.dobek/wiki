# Bash
## Ubuntu Upgrade Packages
### Upgrade

Update the list of available packages and their versions (it does not install or upgrade any packages) :

```
apt-get update
```

Run upgrade with `verbose` mode. Make sure there are no breaking changes, before accepting.
You may also want to save the list of changes, in case you will need to downgrade.

```
apt-get upgrade -V
```

Keep in mind that updating some of the packages (e.g. redis, nginx, …) can make page unavailable for users, for a brief period of time.

Check our monitoring system to see if all of the services are running properly.

### Downgrading

To downgrade package use :

```
apt-get install <package-name>=<package-version-number>
```

To check all available versions :

```
apt-cache showpkg <package-name>
```

To prevent automatic upgrades on package :

```
apt-mark hold <package-name>
```
## Arguments from last command

```
If the previous command had two arguments, like this

ls a.txt b.txt
and you wanted the first one, you could type

!:1
giving

a.txt
Or if you wanted both, you could type

!:1-2
giving

a.txt b.txt
You can extend this to any number of arguments, eg:

!:10-12
```



## tee

tee is normally used to split the output of a program so that it can be both displayed and saved in a file. The command can be used to capture intermediate output before the data is altered by another command or program. The tee command reads standard input, then writes its content to standard output. It simultaneously copies the result into the specified file(s) or variables

```
ls -l | tee file.txt | less

STDIN less
> file.txt
```



## CPU

```
cat /proc/loadavg

9.35 8.73 8.37 9/753 3425
```

```
cat /proc/cpuinfo | grep "model name" | wc -l
```

[proc(5): process info pseudo-file system - Linux man page](https://linux.die.net/man/5/proc)

The first three fields in this file are load average figures giving the number of jobs in the run queue (state R) or waiting for disk I/O (state D) averaged over 1, 5, and 15 minutes. They are the same as the load average numbers given by uptime(1) and other programs.

The fourth field consists of two numbers separated by a slash (/). The first of these is the number of currently executing kernel scheduling entities (processes, threads); this will be less than or equal to the number of CPUs. The value after the slash is the number of kernel scheduling entities that currently exist on the system.

The fifth field is the PID of the process that was most recently created on the system.

[Understanding Linux CPU Load - when should you be worried?](http://blog.scoutapp.com/articles/2009/07/31/understanding-load-averages)
http://www.loadavg.com/2012/01/what-is-loadavg/
0.00 means there’s no traffic on the bridge at all. In fact, between 0.00 and 1.00 means there’s no backup, and an arriving car will just go right on.

1.00 means the bridge is exactly at capacity. All is still good, but if traffic gets a little heavier, things are going to slow down.

over 1.00 means there’s backup. How much? Well, 2.00 means that there are two lanes worth of cars total — one lane’s worth on the bridge, and one lane’s worth waiting. 3.00 means there are three lane’s worth total — one lane’s worth on the bridge, and two lanes’ worth waiting. Etc.


Multi-core systems

To calculate the maximum load value on multi-core systems, the load is relative to the number of cores. On a multicore system, your load should not exceed the number of cores available.

How the cores are spread out over CPUs doesn’t matter. Two quad-cores is the same as four dual-cores which is the same as eight single-cores. It’s all eight cores for these purposes and so the maximum load wold be 8.

The “100% utilization” mark is 1.00 on a single-core system, 2.00, on a dual-core, 4.00 on a quad-core, etc.

Rule of Thumb : The “number of cores = max load”


Well, not exactly. The problem with a load of 1.00 is that you have no headroom. In practice, many sysadmins will draw a line at 0.70:

The "Need to Look into it" Rule of Thumb: 0.70 If your load average is staying above > 0.70, it's time to investigate before things get worse.



## Image compression

```
find ./public/ -type f -name "*.jpg" -exec jpegoptim --strip-all --max=90 {} \;
```

### ImageMagick

#### jpeg

* Reduce quality to 85 if it was higher. With quality larger than 85, the image becomes larger quickly, while the visual improvement is little.
* Reduce Chroma sampling to 4:2:0, because human visual system is less sensitive to colors as compared to luminance.
* Use progressive format for images over 10k bytes. Progressive JPEG usually has higher compression ratio than baseline JPEG for large image, and has the benefits of progressively rendering.
* Use grayscale color space if the image is black and white.

```
convert INPUT.jpg -sampling-factor 4:2:0 -strip [-resize WxH] [-quality N] [-interlace JPEG] [-colorspace Gray/RGB] OUTPUT.jpg
```

#### png

* Always convert GIF to PNG unless the original is animated or tiny (less than a few hundred bytes).
* For both GIF and PNG, remove alpha channel if all of the pixels are opaque.

```
convert INPUT.gif_or_png -strip [-resize WxH] [-alpha Remove] OUTPUT.png
```

#### script

```
find ./public/ -type f -name "*.jpg" | xargs -I % convert -sampling-factor 4:2:0 -strip -quality 90 -interlace Plane % %

find ./public/ -type f -name "*.png" | xargs -I % convert -strip % %
```



## Journalctl

```
journalctl --unit cron.service
journalctl --since "2017-11-29 5:20:00"
```

```
> nginx : show IP with most errors

cat /var/log/nginx/access.log | egrep " 503 [0-9]* " | awk '{ print $1 }' | sort | uniq -c | sort -nr
```



## Postfix
### Logs

```
tail -fn100 /var/log/mail.log
```


### Errors

The mydestination parameter specifies what domains this machine will deliver locally, instead of forwarding to another machine

```
# /etc/postfix/main.cf
# change

mydestination = lemonfrog, $myhostname, lemonfrog-v2, localhost.localdomain, localhost

# to

mydestination = $myhostname, lemonfrog-v2, localhost.localdomain, localhost
```
