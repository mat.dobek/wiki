## Table size

```
SELECT table_schema, table_name, ROUND((data_length+index_length)/POWER(1024,2),2) AS tablesize_mb FROM information_schema.tables ORDER BY tablesize_mb DESC LIMIT 20;
```

## Debugging

```
show status like '%onn%';

+-----------------------------------------------+---------------------+
| Variable_name                                 | Value               |
+-----------------------------------------------+---------------------+
| Aborted_connects                              | 5                   |
| Connection_errors_accept                      | 0                   |
| Connection_errors_internal                    | 0                   |
| Connection_errors_max_connections             | 0                   |
| Connection_errors_peer_address                | 0                   |
| Connection_errors_select                      | 0                   |
| Connection_errors_tcpwrap                     | 0                   |
| Connections                                   | 373287              |
| Locked_connects                               | 0                   |
| Max_used_connections                          | 80                  |
| Max_used_connections_time                     | 2017-10-27 15:23:12 |
| Performance_schema_session_connect_attrs_lost | 0                   |
| Ssl_client_connects                           | 0                   |
| Ssl_connect_renegotiates                      | 0                   |
| Ssl_finished_connects                         | 0                   |
| Threads_connected                             | 53                  |
+-----------------------------------------------+---------------------+
```

```
mysql> SHOW PROCESSLIST;
+--------+-----------+-----------+-----------+---------+--------
| Id     | User      | Host      | db        | Command | Time   |
+--------+-----------+-----------+-----------+---------+--------
| 264306 | xxxxxxxxx | localhost | xxxxxxxxx | Sleep   | 596097 |
| 275863 | xxxxxxxxx | localhost | xxxxxxxxx | Sleep   |   2667 |
| 275864 | xxxxxxxxx | localhost | xxxxxxxxx | Sleep   |   2667 |
| 275865 | xxxxxxxxx | localhost | xxxxxxxxx | Sleep   |   2668 |
| 275866 | xxxxxxxxx | localhost | xxxxxxxxx | Sleep   |   2668 |
| 275867 | xxxxxxxxx | localhost | xxxxxxxxx | Sleep   |   2667 |
| 372377 | xxxxxxxxx | localhost | xxxxxxxxx | Sleep   |   3593 |
| 372378 | xxxxxxxxx | localhost | xxxxxxxxx | Sleep   |      3 |
| 372382 | xxxxxxxxx | localhost | xxxxxxxxx | Sleep   |      3 |
| 372386 | xxxxxxxxx | localhost | xxxxxxxxx | Sleep   |      3 |
| 373222 | root      | localhost | NULL      | Query   |      0 |
```
